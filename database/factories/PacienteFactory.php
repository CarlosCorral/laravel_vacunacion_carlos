<?php

namespace Database\Factories;
use App\Models\Grupo;
use App\Models\Paciente;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
class PacienteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Paciente::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $nombre = $this->faker->name;
        $vacunado=$this->faker->boolean;
        if($vacunado==true){
            $fechaVacuna=$this->faker->dateTimeThisYear;  
        }else{
            $fechaVacuna=null;
        }
        $grupo_id=Grupo::all()->random()->id;

        return [
            'nombre' => $nombre,
            'slug' => Str::slug($nombre),
            'vacunado'=>$vacunado,
            'fechaVacuna'=>$fechaVacuna,
            'grupo_id'=>$grupo_id
        ];
    }
}
