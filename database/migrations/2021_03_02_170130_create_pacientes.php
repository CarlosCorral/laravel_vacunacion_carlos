<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePacientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacientes', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->max(150);
            $table->string('slug')->unique;
            $table->boolean("vacunado")->default(false);
            $table->string("fechaVacuna")->nullable();
            $table->unsignedBigInteger('grupo_id');
            $table->foreign("grupo_id")->references("id")->on("grupos");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacientes');
    }
}
