<?php

namespace Database\Seeders;
use App\Models\Paciente;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('grupos')->delete();
        $this->call(GrupoSeeder::class);

        DB::table('vacunas')->delete();
        $this->call(VacunaSeeder::class);

        Paciente::factory(25)->create();
    }
}
