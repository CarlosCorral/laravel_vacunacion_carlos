<?php

namespace App\Models;
use App\Models\Vacuna;
use App\Models\Paciente;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Grupo extends Model
{
    use HasFactory;
    protected $table = "Grupos";
    public function vacunas() {
        return $this->belongsToMany(Vacuna::class);
    }
    public function pacientes() {
        return $this->hasMany(Paciente::class);
    }
}
