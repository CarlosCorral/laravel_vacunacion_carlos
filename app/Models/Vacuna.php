<?php

namespace App\Models;
use App\Models\Grupo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vacuna extends Model
{
    use HasFactory;
    protected $table = "Vacunas";
    public function grupos(){
        return $this->belongsToMany(Grupo::class);
    }

    public function getRouteKeyName() {
        return 'slug';
    }
}
