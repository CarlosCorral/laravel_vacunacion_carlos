<?php

namespace App\Models;
use  App\Models\Grupo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    use HasFactory;
    protected $table = "Pacientes";
    public function grupo() {
        return $this->belongsTo(Grupo::class);
    }
}
