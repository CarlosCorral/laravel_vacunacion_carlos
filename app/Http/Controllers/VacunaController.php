<?php

namespace App\Http\Controllers;
use App\Models\Vacuna;
use App\Models\Paciente;
use Illuminate\Http\Request;

class VacunaController extends Controller
{
    public function inicio(){
    return redirect()->route("vacunas.index");
    }

    public function index(){
        $vacunas= Vacuna::all();
		return view('vacunas.index',["vacunas"=>($vacunas)]);
    }
    public function show(Vacuna $vacuna){
        return view("vacunas.show", ["vacuna" => $vacuna]);
    }

    public function vacunar(Paciente $paciente){
       $paciente->vacunado=true;
       //$paciente->fechaVacuna=;
       $paciente->save();
       return redirect()->back();
    }
    public function buscador(){
        return view("pacientes.buscador");
    }
}


