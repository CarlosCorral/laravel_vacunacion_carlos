<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PacienteController extends Controller
{

    public function buscador(){
        return view("pacientes.buscador");
    }

    public function buscar(Request $request) {
         
        $pacientes = Paciente::where("nombre", "like", "%".$request->buscador ."%")->pluck('nombre');
         
         return response()->json($pacientes);     
     }
}
