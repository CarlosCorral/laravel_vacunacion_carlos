@extends('layouts.master')
@section('titulo')
Vacunación
@endsection
@section('contenido')
<div class="row">
    @foreach( $vacunas as $vacuna )
    <div class="col-xs-12 col-sm-6 col-md-4 ">
    <h4 style="min-height:45px;margin:5px 0 10px 0">
    {{$vacuna->nombre}}
    </h4>
    Posibles grupos de vacunación:
    <ul>
    @foreach($vacuna->grupos as $grupo)
    <li>{{$grupo->nombre}}</li>
    @endforeach
    </ul>
    </div>
    @endforeach
</div>
@endsection