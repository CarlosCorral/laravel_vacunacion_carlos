@extends('layouts.master')
@section('titulo')
    Vacunación
@endsection
@section('contenido')
<h1>{{$vacuna->nombre}}</h1>
<h2>Pacientes no vacunados</h2>
 <table>
     <tr>
        <th>Nombre</th>
        <th>Grupo de vacunación</th>
        <th>Prioridad</th>
        <th>Acción</th>
     </tr>
        @foreach($vacuna->grupos as $grupo)
            @foreach($grupo->pacientes as $paciente)
                @if($paciente->vacunado == false)
                    <tr>
                        <td>{{$paciente->nombre}}</td>
                        <td>{{$paciente->grupo->nombre}}</td>
                        <td>{{$paciente->grupo->prioridad}}</td>
                        <td><a href="{{route('vacunar' , $paciente )}}"><button type="button" class="btn btn-warning">Vacunar</button></a></td>
                    </tr>
                @endif
            @endforeach
        @endforeach
 </table>
 <h2>Pacientes vacunados</h2>
 <table>
   @foreach($vacuna->grupos as $grupo)
   <tr>
    <th>{{$grupo->nombre}}</th>
   </tr>
   <tr>
    <td>
      <ul>
        @foreach($grupo->pacientes as $paciente)
        <li>{{$paciente->nombre}}   {{$paciente->fechaVacuna}}</li>
        @endforeach
      </ul>
    </td>
   </tr>
   @endforeach
</table>
@endsection

